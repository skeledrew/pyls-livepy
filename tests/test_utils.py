# Copyright (c) 2021  Andrew Phillips <skeledrew@gmail.com>

#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

"""


import doctest

from pyls_livepy import utils, runner


def parametrize_with_doctests(module):

    def inner(func):
        doctests = doctest.DocTestFinder().find(module)
        runit = runner.CapDocTestRunner(verbose=True)
        results = []

        for dt in doctests:
            if not dt.examples:
                continue
            co = runner.capout()
            runit.run(dt, out=co)
            results += co()
        func.dt_keys = "test,example,want,got,exc_msg"
        func.dt_results = [
            (
                r["test"],
                r["example"],
                r["example"].want,
                r["got"],
                r["example"].exc_msg,
            )
            for r in results
        ]
        func.dt_ids = [
            f'{r["test"].name.rpartition(".")[2]}'
            + f':{r["test"].lineno + r["example"].lineno + 1}'
            for r in results
        ]
        return func
    return inner


def pytest_generate_tests(metafunc):
    fn = metafunc.function
    if getattr(fn, 'dt_results', None):
        metafunc.parametrize(fn.dt_keys, fn.dt_results, ids=fn.dt_ids)
    return


@parametrize_with_doctests(utils)
def test_utils_doctests(test, example, want, got, exc_msg):
    assert want == got
    return
