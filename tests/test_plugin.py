# Copyright (c) 2021  Andrew Phillips <skeledrew@gmail.com>

#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

"""


from os import environ as oenv
from pathlib import Path
import pkg_resources
import types

import pytest

from pyls_livepy import plugin


here = Path(__file__).parent
plugin.logger.remove()  # disable file logging during testing


def set_mock_attr(m, name, value):
    """Set an attribute and return the object."""
    # TODO: find how to make attrib drilling passable
    setattr(m.__truediv__().exists, name, value)
    return m


@pytest.fixture
def datadir():
    return here / "data"


def test_pyls_lint(monkeypatch, mocker, src, datadir, doc_nt):
    doc = doc_nt(src, datadir / "sample.py")
    fpr_m = mocker.MagicMock()
    monkeypatch.setattr(plugin, "find_project_root", fpr_m)
    pc_m = mocker.MagicMock(return_value={"env": ".myenv"})
    monkeypatch.setattr(plugin, "parse_config", pc_m)
    sll_m = mocker.Mock()
    monkeypatch.setattr(plugin, "set_log_level", sll_m)
    entem_m = mocker.Mock()
    monkeypatch.setattr(plugin, "ensure_nox_test_env_maker", entem_m)
    re_m = mocker.Mock(return_value="/path/to/.myenv")
    monkeypatch.setattr(plugin, "resolve_env", re_m)
    rtie_m = mocker.Mock(return_value=[])
    monkeypatch.setattr(plugin, "run_tests_in_env", rtie_m)
    plugin.pyls_lint(doc)
    fpr_m.assert_called_with(doc.path, "pyproject.toml")
    pc_m.assert_called()
    re_m.assert_called_with(".myenv", fpr_m())
    d_path = datadir / "sample.py"
    rtie_m.assert_called_with("/path/to/.myenv", src, "warning", d_path)
    return


@pytest.mark.parametrize("fpr_mm, c_rv", [
    (
        lambda m: m.Mock(return_value=None),
        lambda rv: len(rv) == 1 and rv[0]["message"].startswith("unable to"),
    ),
    (
        lambda m: m.Mock(side_effect=Exception()),
        lambda rv: len(rv) == 1 and rv[0]["message"].startswith("Exception"),
    ),
    (
        lambda m: m.Mock(side_effect=lambda p, s: ""),
        lambda rv: len(rv) == 1 and rv[0]["message"].startswith("TypeError"),
    ),
    (
        lambda m: m.Mock(
            side_effect=lambda p, s: set_mock_attr(
                m.MagicMock(),
                "return_value",
                True,
            ),
        ),
        lambda rv: rv == [],
    ),
], ids=["LivepyError", "Exception", "KeyError", "disabled"])
def test_pyls_lint_branches(monkeypatch, mocker, src, doc_nt, fpr_mm, c_rv):
    doc = doc_nt(src, "")
    fpr_m = fpr_mm(mocker)
    monkeypatch.setattr(plugin, "find_project_root", fpr_m)
    pc_m = mocker.MagicMock(return_value={"ensure_nox_test_env_maker": False})
    monkeypatch.setattr(plugin, "parse_config", pc_m)
    sll_m = mocker.Mock()
    monkeypatch.setattr(plugin, "set_log_level", sll_m)
    entem_m = mocker.Mock()
    monkeypatch.setattr(plugin, "ensure_nox_test_env_maker", entem_m)
    re_m = mocker.Mock(side_effect=KeyError())
    monkeypatch.setattr(plugin, "resolve_env", re_m)
    rv = plugin.pyls_lint(doc)
    assert c_rv(rv)
    return


def test_pyls_on_type_formatting(src, datadir, doc_nt):
    doc = doc_nt(src, datadir / "sample.py")
    assert plugin.pyls_on_type_formatting(doc) == []
    return


def test_mark_err(src):
    msg = "test message"
    rv = plugin._mark_err(src, msg)
    assert isinstance(rv, dict)
    assert rv["message"] == msg and rv["range"]["end"]["line"] == 16
    return


@pytest.mark.parametrize(
    "rel_path, e_path",
    [
        ("~/myenv", Path().home() / "myenv/bin/activate"),
        (".myenv", here / "data/.myenv/bin/activate"),
        ("myenv/py*", here / "data/myenv/py37/bin/activate"),
        ("activate", here / "data/activate"),
    ],
    ids=["home_based", "project_based", "glob", "direct"],
)
def test_resolve_env(rel_path, e_path, datadir):
    rv = plugin.resolve_env(rel_path, datadir)
    assert str(rv) == str(e_path)
    return


@pytest.mark.skipif(
    not oenv.get("CONDA_PREFIX", None)
    or not (
        Path(oenv["CONDA_PREFIX"]).parent / "pyls_livepy_test_env"
    ).is_dir(),
    reason="Requires active conda and env 'pyls_livepy_test_env'",
)
def test_resolve_env_with_conda(datadir, tmpdir):
    env_name = "pyls_livepy_test_env\n"
    act_path = Path(tmpdir.mkdir("project").join("activate-conda"))
    act_path.write_text(env_name)
    rel_path = "activate-conda"
    e_path = Path(oenv["CONDA_PREFIX"]).parent / env_name.strip()
    rv = plugin.resolve_env(rel_path, act_path.parent)
    assert str(rv.resolve()) == str(e_path) and rv.is_dir()
    return


@pytest.mark.skipif(
    not (here / "../.nox").is_dir(),
    reason="A .nox env folder is missing. Run 'nox'",
)
def test_run_tests_in_env(src, datadir):
    env = plugin.resolve_env(".nox/make_*", here / "..")
    rv = plugin.run_tests_in_env(env, src, "warning")
    assert isinstance(rv["markers"], list) and len(rv["markers"]) == 2
    e_msg = "unsupported operand type(s) for ** or pow(): 'str' and 'int'"
    assert rv["markers"][1]["message"] == e_msg
    return


def test_find_project_root():
    rv = plugin.find_project_root(__file__, "pyproject.toml")
    assert rv.name == "pyls-livepy"
    return


def test_parse_config():
    ENV = ".tox/py*"
    content = f"""
[tool.pyls-livepy]
env = "{ENV}"
    """
    rv = plugin.parse_config(content)
    assert isinstance(rv, dict) and rv["env"] == ENV
    return


@pytest.mark.skipif(
    "site-packages/pyls_livepy" not in plugin.__file__,
    reason=f"Project must be installed; testing {plugin.__file__}",
)
def test_entry_point():
    distribution = pkg_resources.get_distribution("pyls-livepy")
    entry_point = distribution.get_entry_info("pyls", "pyls_livepy")

    assert entry_point is not None

    module = entry_point.load()
    assert isinstance(module, types.ModuleType)
    return
