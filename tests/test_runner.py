# Copyright (c) 2021  Andrew Phillips <skeledrew@gmail.com>

#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

"""


import json
from pathlib import Path
import subprocess as sp

import pytest

from pyls_livepy import __version__ as plp_version, runner, utils


here = Path(__file__).parent
runner.logger.remove()  # disable file logging during testing


def test_run_doctests(src):
    rv = runner.run_doctests(src, "")
    out = rv["rdt_mut.square"]["out"]
    assert isinstance(rv, dict) and all(
        [isinstance(e, dict) and "example" in e for e in out]
    )
    return


def test_create_markers(src):
    rv = runner.create_markers(runner.run_doctests(src, ""))
    assert isinstance(rv, list) and all(
        [isinstance(e, dict) and "severity" in e for e in rv]
    )
    return


def test_run(mocker, monkeypatch):
    cm_m = mocker.Mock(return_value=None)
    monkeypatch.setattr(runner, "create_markers", cm_m)
    rd_m = mocker.Mock(return_value={})
    monkeypatch.setattr(runner, "run_doctests", rd_m)
    gs_m = mocker.Mock(
        return_value=json.dumps({
            "source": "",
            "plp_version": plp_version,
            "log_level": "warning",
            "mut_path": "",
        })
    )
    monkeypatch.setattr(runner, "_get_stdin", gs_m)
    ps_m = mocker.Mock()
    monkeypatch.setattr(runner, "_put_stdout", ps_m)
    runner.run()
    cm_m.assert_called_once()
    rd_m.assert_called_once()
    gs_m.assert_called_once()
    ps_m.assert_called_once()
    return


@pytest.mark.skipif(
    "site-packages/pyls_livepy" not in runner.__file__,
    reason=f"Project must be installed; testing {runner.__file__}",
)
def test_script_run():
    src = '''
def func(x):
    """
    >>> func(5)
    0
    """
    return x * x
    '''.strip()
    args = ["pyls-livepy-runner"]
    data = json.dumps({
        "source": src,
        "plp_version": plp_version,
        "log_level": "warning",
        "mut_path": "",
    })
    cp = sp.run(args, capture_output=True, input=data, text=True)
    assert cp.stdout and not cp.stderr
    rv = json.loads(cp.stdout)
    assert rv["markers"][0]["message"] == "failure in doctest; see log"
    return


@pytest.mark.parametrize("path, check_markers", [
    (
        here.parent.parent / "Inssist/hoverin/hoverin/gen_test.py",
        lambda ms: len(ms) == 0,
    ),
], ids=["gen_test"])
def test_run_external_sample(monkeypatch, path, check_markers):
    """Run on files in external projects."""
    if not path.exists():
        pytest.skip()
        return
    from pprint import pprint
    import sys

    old_paths = sys.path
    pprint(old_paths)
    PYPT = "pyproject.toml"
    proot = utils.find_project_root(path, PYPT)
    config = utils.parse_config((proot / PYPT).read_text())
    env_path = utils.resolve_env(config["env"], proot)
    print(proot, env_path)
    new_paths = []
    old_env_path = str(Path([p for p in sys.path if p.endswith("/bin")][0]).parent)
    new_paths += [p.replace(old_env_path, str(env_path)).replace("/python3.9", "/python3.8") for p in old_paths if old_env_path in p and Path(p).exists()]
    old_proot = str(here.parent)
    new_paths += [p.replace("/src", "").replace(old_proot, str(proot)) for p in old_paths if old_proot in p and not f"{old_proot}." in p]
    print(new_paths)
    monkeypatch.setattr(sys, "path", new_paths + old_paths)
    src = path.read_text()
    markers = runner.create_markers(runner.run_doctests(src, ""))
    c_rv = check_markers(markers)
    if not c_rv:
        pprint(new_paths)
        pprint(markers)
    assert c_rv is True
    return


def test_maybe_update(mocker):
    dt_m = mocker.Mock()
    ex1_m = mocker.Mock()
    ex1_m.source = "from pyls_livepy import run_pytest_case"
    ex2_m = mocker.Mock()
    ex2_m.source = "run_pytest_case('')"
    ex3_m = mocker.Mock()
    ex3_m.source = "my_func()"
    dt_m.examples = [ex1_m, ex2_m, ex3_m]
    rpc_m = mocker.Mock()
    rpc_m.name = "pyls_livepy.run_pytest_case"
    dt_m.globs = {
        "run_pytest_case": rpc_m,
    }
    rv = runner._maybe_update(dt_m, True)
    assert rv is True
    assert ex2_m.want == ""
    return


def test_run_pytest_case(monkeypatch, src):
    import types

    t_mod = types.ModuleType("test_mod")
    exec(src, t_mod.__dict__)
    runner.run_pytest_case.set_mut(t_mod)

    with pytest.raises(runner.PytestException, match="AttributeError: module"):
        runner.run_pytest_case("-k run_doctests --no-cov")
    import _pytest
    reports = _pytest.main.Session._reportscap()[0]
    assert len(reports) == 3 and reports[1].outcome == "failed"
    return
