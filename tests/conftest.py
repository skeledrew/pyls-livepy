# -*- coding: utf-8 -*-

# Copyright (c) 2021  Andrew Phillips <skeledrew@gmail.com>

#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Test fixtures and config for pyls_livepy.
"""


from collections import namedtuple as nt

import pytest


@pytest.fixture
def src():
    return '''
def square(x):
    """Return the square of 'x'

        >>> square(0)
        0
        >>> square(1)
        1
        >>> square(2)
        4
        >>> square(3)
        7
        >>> square(4)
        16
        >>> square("5")
        25
    """
    return x ** 2
    '''.strip()


@pytest.fixture
def doc_nt():
    return nt("Document", ["source", "path"])
