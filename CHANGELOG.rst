=========
Changelog
=========

0.2.1 (2021-01-17)
==================

Fixed
-----

- Pytest case runner

0.2.0 (2021-01-15)
==================

Added
-----

- Pytest case runner

Changed
-------

- Doctest exception marker now gives actual message

Fixed
-----

- Version mismatch marker

0.1.1 (2021-01-01)
==================

Fixed
-----

- PyPI metadata
- GitLab CI/CD
- External samples test

0.1.0 (2020-12-31)
==================

- Initial release
